# IO-Link ISDU Data via a CODESYS EIP Scanner

## Project Details

- [ ] Written in Structured Text
- [ ] Codesys Version 3.5.19.20
- [ ] Target: PFC200 750-8216 FW26

## Name
PFC200 EIP IOL ISDU RW Example

## Description
In this example, a WAGO 750-8216 controller is programmed to be an EtherNet/IP scanner.  It communicates via EtherNet/IP to a 750-4501/100-000 System Field IO-Link Master.  Connected to Port 1 of the 765-4501 is a 765-2704/200-000 2-Channel Analog Output IO-Link Converter.  The CODESYS project reads and writes parameters of the 765-2704 via EtherNet/IP CIP services.

![Program](CODESYS_EIP_ISDU.png){width=500 height=300}

## Support
This program is for demonstration only and does not include support.  May contain bugs.  Use at your own risk.

